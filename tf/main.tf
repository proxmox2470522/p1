terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.11"
    }
  }
  required_version = ">= 0.13"
}

provider "proxmox" {
  pm_api_url          = "https://pve1:8006/api2/json"
  pm_api_token_id     = "example@pam!new_token_id"
  pm_api_token_secret = "8cd645fe-3ffd-4ebd-b3e0-ca8eb08c5800"
  pm_tls_insecure     = true
}

resource "proxmox_vm_qemu" "test_server" {
  count       = length(var.hostnames)
  name        = var.hostnames[count.index]
  target_node = var.proxmox_host["target_node"]
  vmid        = var.vmid + count.index
  full_clone  = true
  clone       = var.template_name
  cores = 2
  sockets = 2
  vcpus = 4
  memory = 4096
  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  ipconfig0 = "ip=${var.ips[count.index]}/24,gw=${cidrhost(format("%s/24", var.ips[count.index]), 1)}"

  lifecycle {
    ignore_changes = [
    network,
    ]
  }  
  connection {
    host = var.ips[count.index]
    user = var.user
    private_key = file(var.ssh_keys["priv"])
    agent = false
    timeout = "3m"
  }

  provisioner "remote-exec" {
	  # Leave this here so we know when to start with Ansible local-exec 
    inline = [ "echo 'Cool, we are ready for provisioning'"]
  }

  provisioner "local-exec" {
      working_dir = "../ansible/"
#      command = "ansible-playbook -u ${var.user} --key-file ${var.ssh_keys["priv"]} -i ${var.ips[count.index]}, pb.yml"
      command = "ansible-playbook -u ${var.user} --key-file ${var.ssh_keys["priv"]} -i inventory pb.yml"
  }
}
