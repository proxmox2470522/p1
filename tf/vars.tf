variable "proxmox_host" {
  type = map(any)
  default = {
    pm_api_url  = "https://10.2.2.101:8006/api2/json"
    pm_user     = "example@pam"
    target_node = "pve1"
  }
}

variable "vmid" {
  default     = 400
  description = "Starting ID for the CTs"
}

variable "ssh_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDkc1WyLK5cNcyOJqyViH/iRxol1kH+j8v/gfuKAobw8kx+KRW1u6ow4TUIOEYRzQ4XfW3QQq9tWJUlUxMF9legWXOlmdlititpZ5MHtY9tprzy0+sudUgk6kwFwxBZdrZOzE5r4KjdYM9zGvW9PvtTYvhYTu6x/jwFLZ0luRtJNYvynXD70kczMqrCsFhxdkYEbqwum2V3JhB8LIUPGo63MRnkWeJ2KShNa6Aq2CMKFzX8/+wus2irXis9gZ0t8s4KaAH9n8dZBmXB4DgdEWifhvvO9g22C/00aZ6I9XWqN1Z14lpphEBEsoCBY8+n6qoSDjhEG2tqeZX2n+XEZ8qx petr@centos"
}

variable "template_name" {
  default = "bullseye-cloudinit"
}

variable "hostnames" {
  description = "Virtual machines to be created"
  type        = list(string)
  default     = ["master", "node1", "node2"]
}

variable "ips" {
  description = "IPs of the VMs, respective to the hostname order"
  type        = list(string)
  default     = ["10.2.2.41", "10.2.2.42", "10.2.2.43"]
}

variable "ssh_keys" {
  type = map(any)
  default = {
    pub  = "~/.ssh/id_rsa.pub"
    priv = "~/.ssh/id_rsa"
  }
}

variable "user" {
  default     = "petr"
  description = "User used to SSH into the machine and provision it"
}